package com.example.nv_muse_ceri.data;

import androidx.annotation.NonNull;
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;
@Entity(tableName = "muse_table", indices = {@Index(value = {"name", "description"},
        unique = true)})

public class Muse {


        @PrimaryKey
        @NonNull
        @ColumnInfo(name="id")
        private String id;

        @NonNull
        @ColumnInfo(name="name")
        private String name;

        @NonNull
        @ColumnInfo(name="description")
        private String description=null;



        @NonNull
        @ColumnInfo(name="brand")
        private String brand="Inconnu";

        @NonNull
        @ColumnInfo(name="working")
        private boolean working=false;

        @NonNull
        @ColumnInfo(name="year")
        private int year=0;


        public Muse() {

        }


    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }

    @NonNull
    public String getBrand() {
        return brand;
    }

    public void setBrand(@NonNull String brand) {
        this.brand = brand;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}

