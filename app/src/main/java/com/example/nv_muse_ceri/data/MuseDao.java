package com.example.nv_muse_ceri.data;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
@Dao
public interface MuseDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Muse muse);
    @Query("SELECT * FROM muse_table WHERE id = :id")
    Muse getMuseById(String id);

    @Query("SELECT * from muse_table ORDER BY name ASC")
    List<Muse> getMuseAll();
    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Muse muse);





}
