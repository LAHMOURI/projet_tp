package com.example.nv_muse_ceri.data;

import java.util.ArrayList;

public class MuseResult {
    public static void transferInfo(MuseResponse body, String key, ArrayList<Muse> muses) {

        Muse muse = new Muse();
        muse.setId(key);
        muse.setName(body.name);
        muse.setDescription(body.description);
        muse.setWorking(body.working);
        //muse.setCategories(body.categories);
        if (body.year!=null){
            muse.setYear(body.year);
        }
        if (body.brand!=null){
            muse.setBrand(body.brand);
        }

        System.out.println(muse.getName()+"==="+muse.isWorking());
        muses.add(muse);

    }
}
