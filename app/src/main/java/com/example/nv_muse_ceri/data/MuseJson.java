package com.example.nv_muse_ceri.data;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface MuseJson {
    @Headers({
            "Accept: json",
    })
    @GET("collection")
    public Call<Map<String,MuseResponse>> getCollection();
}
