package com.example.nv_muse_ceri.data;
import androidx.annotation.NonNull;
import androidx.room.Database;
import android.content.Context;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import com.example.nv_muse_ceri.data.MuseDao;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import com.example.nv_muse_ceri.data.Muse;

@Database(entities = {Muse.class}, version = 4, exportSchema = false)
public abstract class MuseRoomDatabase  extends RoomDatabase {
    private static final String TAG = MuseRoomDatabase.class.getSimpleName();
    public abstract MuseDao museDao();

    private static MuseRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static MuseRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MuseRoomDatabase.class) {
                if (INSTANCE == null) {

                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    MuseRoomDatabase.class,"muse_table")
                                    .fallbackToDestructiveMigration()
                                    .build();
                }
            }
        }
        return INSTANCE;
    }
    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {  MuseDao dao = INSTANCE.museDao();


                    });

                }
            };






}
