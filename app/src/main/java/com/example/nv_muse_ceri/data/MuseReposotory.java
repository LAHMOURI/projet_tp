package com.example.nv_muse_ceri.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static com.example.nv_muse_ceri.data.MuseRoomDatabase.databaseWriteExecutor;

public class MuseReposotory {
    private static final String TAG = MuseReposotory.class.getSimpleName();

    private MutableLiveData<Muse> sel_muse =new MutableLiveData<>();
    private final MuseJson api;
    public MutableLiveData<Boolean> load =new MutableLiveData<Boolean>();
    public MutableLiveData<Throwable> webServiceThrowable=new MutableLiveData<>();
    private MuseDao museDao;
    volatile int nbLoads =0;
    private MutableLiveData<ArrayList<Muse>> muses =new MutableLiveData<>();
    public MutableLiveData<ArrayList<Muse>> getMuses() {
        return muses;
    }
    private static volatile MuseReposotory INSTANCE;

    public synchronized static MuseReposotory get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseReposotory(application);
        }
        return INSTANCE;
        }
    public MuseReposotory(Application application) {
        MuseRoomDatabase db = MuseRoomDatabase.getDatabase(application);
        museDao =db.museDao();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(MuseJson.class);


    }
    public MutableLiveData<Muse> getSel_muse() { return sel_muse; }
    public void loadCollectionFromDatabase(){
        ArrayList<Muse> allMuses= (ArrayList<Muse>) museDao.getMuseAll();
        muses.postValue(allMuses);
    }
    public void loadCollection(){
        load.postValue(Boolean.TRUE);
        api.getCollection().enqueue(
                new Callback<Map<String, MuseResponse>>() {
                    @Override
                    public void onResponse(Call<Map<String, MuseResponse>> call,
                                           Response<Map<String, MuseResponse>> response) {

                    ArrayList<Muse> MuseeTMP=new ArrayList<>();
                    int H=0;
                    for (String key:response.body().keySet()) {
                        MuseResult.transferInfo(response.body().get(key), key, MuseeTMP);
                        long res= insertMuse(MuseeTMP.get(H));
                        H=H+1;

                                                               }
                      muses.setValue(MuseeTMP);
                      load.postValue(Boolean.FALSE);
                                                          }

        @Override
        public void onFailure(Call<Map<String, MuseResponse>> call, Throwable t) {
            Log.d("PROBLEME API",t.getMessage());
            if (nbLoads <=1){
                load.postValue(Boolean.FALSE);
            }
            else{
                nbLoads = nbLoads -1;
            }
            webServiceThrowable.postValue(t);
        }
    });
}

    public long insertMuse(Muse newMuse) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return museDao.insert(newMuse);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }
    public void getMuse(String id)  {

        Future<Muse> F = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return museDao.getMuseById(id);
        });
        try {
            sel_muse.setValue(F.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}









