package com.example.nv_muse_ceri;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.nv_muse_ceri.data.Muse;
import com.example.nv_muse_ceri.data.MuseReposotory;

import java.util.ArrayList;

public class ListViewModel extends AndroidViewModel {
        private MuseReposotory repository;
        private MutableLiveData<Boolean> isLoading;

        private MutableLiveData<ArrayList<Muse>> allMuses;

    public ListViewModel (Application application) {
            super(application);
            //repository.loadCollection();
            ArrayList<Muse> var=new ArrayList<Muse>();
            allMuses =new MutableLiveData<>();
            repository = MuseReposotory.get(application);
            isLoading=repository.load;

            allMuses =repository.getMuses();
        }




        LiveData<Boolean> getIsLoading() {
            return isLoading;
        }

        LiveData<ArrayList<Muse>> getAllMuses() {
            return allMuses;
        }

        public void loadCollection(){

            Thread t = new Thread(){
                public void run(){
                    repository.loadCollection();

                }
            };
            t.start();

        }

        public void loadCollectionFromDatabase(){

            Thread t = new Thread(){
                public void run(){
                    repository.loadCollectionFromDatabase();

                }
            };
            t.start();

        }

}
