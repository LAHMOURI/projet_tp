package com.example.nv_muse_ceri;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {



    ListViewModel listViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listViewModel = new ViewModelProvider(this).get(ListViewModel.class);
        listViewModel.loadCollection();
        listViewModel.loadCollectionFromDatabase();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.context_menu, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update_all) {
            /*
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Recherche d'antiquité",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

             */
            listViewModel.loadCollection();
            //View view=findViewById(android.R.id.content);
            System.out.println("FINI");

            return true;
        }
       // if (id == R.id.action_remove_all) {
            /*
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Recherche d'antiquité",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

             */
         //   listViewModel.removeAll();
            //View view=findViewById(android.R.id.content);
            System.out.println("FINI");

          // return true;
       // }
        return super.onOptionsItemSelected(item);
    }
}