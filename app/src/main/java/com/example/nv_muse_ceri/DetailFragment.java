package com.example.nv_muse_ceri;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;


import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    private ProgressBar progress;
    public static final String TAG = DetailFragment.class.getSimpleName();
    private DetailViewModel viewModel;
    private TextView textName, textDescription, textBrand,textCategorie,textYear;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        return inflater.inflate(R.layout.fragement_details, container, false);
    }
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


            viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

          DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
            String Id = args.getItemNum();
           viewModel.setMuse(Id);

            listenerSetup();
            observerSetup();

        }
    private void listenerSetup() {
        textName = getView().findViewById(R.id.name);
        textDescription = getView().findViewById(R.id.description);
        textBrand = getView().findViewById(R.id.tbrand);
        textCategorie = getView().findViewById(R.id.tcategories);
        textYear = getView().findViewById(R.id.tyear);


        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
          @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
            viewModel.getMuse().observe(getViewLifecycleOwner(),
                    muse -> {
                        if (muse != null) {
                            Log.d(TAG, "observing musee view");

                            textName.setText(muse.getName());
                            textDescription.setText(muse.getDescription());
                            textBrand.setText(muse.getBrand());


                            if(muse.getYear()==-1 || muse.getYear()==0){
                                textYear.setText("Pas mentionné");
                            }
                            else{
                                textYear.setText(String.valueOf(muse.getYear()));
                            }

                            textCategorie.setText(muse.getBrand());

                        }
                    });

            viewModel.getWebService().observe(getViewLifecycleOwner(),
                throwable ->{
                    Snackbar snackbar = Snackbar
                            .make(getView(), throwable.getMessage(), Snackbar.LENGTH_LONG);
                    snackbar.show();

                }
            );
        }


    }

















