package com.example.nv_muse_ceri;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.nv_muse_ceri.data.Muse;
import com.example.nv_muse_ceri.data.MuseReposotory;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();
    private MuseReposotory repository;
    private MutableLiveData<Muse> muse;
    private MutableLiveData<Boolean> load;

    private  MutableLiveData<Throwable> webService;


    public DetailViewModel (Application application) {
        super(application);
        load =new MutableLiveData<Boolean>();
        repository = MuseReposotory.get(application);
        load =repository.load;
        webService =repository.webServiceThrowable;
        muse = new MutableLiveData<>();
                                                 }

    public void setMuse(String id) {
        repository.getMuse(id);
        muse = repository.getSel_muse();

    }
    LiveData<Muse> getMuse() {
        return muse;
    }

    LiveData<Boolean> getLoad() {
        return load;
    }

    LiveData<Throwable> getWebService(){return webService;}

}
