package com.example.nv_muse_ceri;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


import  com.example.nv_muse_ceri.data.Muse;

public class RecyclerAdapter extends RecyclerView.Adapter<com.example.nv_muse_ceri.RecyclerAdapter.ViewHolder> {

    private static final String TAG = com.example.nv_muse_ceri.RecyclerAdapter.class.getSimpleName();
    private ListViewModel listViewModel;

    private static int nb=0;
    private ArrayList<Muse> museList;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v=null;
        if(nb%2==0) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
        }else{
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
        }
        nb++;
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(museList.get(i).getName());
        viewHolder.itemDetail.setText(museList.get(i).getDescription());
    }

    @Override
    public int getItemCount() {

        return museList == null ? 0 : museList.size();
    }

    public void setMuseList(ArrayList<Muse> muse) {
        museList = muse;
        notifyDataSetChanged();
    }


    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }


    public void setMusesList(ArrayList<Muse> muses) {
        museList =muses;
        notifyDataSetChanged();
    }



    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;


        ActionMode actionMode;
        String idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);


            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.context_menu, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    return false;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Log.d(TAG,"position="+getAdapterPosition());
                    String id = RecyclerAdapter.this.museList.get((int)getAdapterPosition()).getId();
                    Log.d(TAG,"id="+id);

                    ListFragmentDirections .ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(id);
                    action.setItemNum(id);
                    Navigation.findNavController(v).navigate(action);

                }
            });


            itemView.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    idSelectedLongClick = RecyclerAdapter.this.museList.get((int)getAdapterPosition()).getId();
                    if (actionMode != null) {
                        return false;
                    }
                    Context context = v.getContext();
                    // Start the CAB using the ActionMode.Callback defined above
                    actionMode = ((Activity)context).startActionMode(actionModeCallback);
                    v.setSelected(true);
                    return true;
                }
            });
        }




    }

}